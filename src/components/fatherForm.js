import React,{Component} from "react";
import Form1 from "./form";

class FatherForm  extends Component {
    constructor(props){
        super(props);
        this.state = {
            SignUp: false,
            logIn: true
        }
    }

    displaySignUpForm = () => {
       this.setState({
            SignUp: true,
            logIn: false
       })
    }
    displayLoginForm = () => {
        this.setState({
            SignUp: false,
            logIn: true
        })
    }
    render(){
        return(
            <React.Fragment>
                <Form1 onClickLoginForm = {this.displayLoginForm} onClickSignUpForm = {this.displaySignUpForm} displaySignUpBoolean = {this.state.SignUp} displayLogInBoolean = {this.state.logIn}/>
            </React.Fragment>
        )
    }
}

export default FatherForm