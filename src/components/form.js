import {  useState } from "react";
import { Container, Col, Row, FormGroup, Input, Button, Form, Label } from "reactstrap";
import "../App.css"
function Form1 (props){ 
    // Data form Login
    const [EmailLogIn, setEmailLogIn] = useState("");
    const [PasswordLogIn, setPasswordLogIn] = useState("");
    // Data form Sign up
    const [FisrtNameSignUp, setFirstNameSignUp] = useState("");
    const [LastNameSignUp, setLastNameSignUp] = useState("");
    const [EmailSignUp, setEmailSignUp] = useState("");
    const [PasswordSignUp, setPasswordSignUp] = useState("");
    // hàm  onchange form login
    const  onChangeEmail = (event) => {
        setEmailLogIn(event.target.value);
    }
    const onChangePassword = (event) => {
        setPasswordLogIn(event.target.value);
    }
    const onChangeLastNameSignUp = (event) => {
        setLastNameSignUp(event.target.value);
    }
    const onChangeFirstNameSignUp = (event) => {
        setFirstNameSignUp(event.target.value);
    }
    const onChangeEmailSignUp = (event) => {
        setEmailSignUp(event.target.value);
    }
    const onChangePasswordSignUp = (event) => {
        setPasswordSignUp(event.target.value);
    }
    // hàm khi nút login được click  
    const onLogIn = (event) => {
        event.preventDefault();
        //validate Email
        if(EmailLogIn.match(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/) === null){
            alert("Email không hợp lệ!")
            return false
        }
        //validate Password
        if(PasswordLogIn.match(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/) === null){
            alert("Password không hợp lệ!")
            return false
        }
        //đẩy thông tin lên local storage nếu thông tin hợp lệ
        localStorage.setItem("PasswordLogin", PasswordLogIn);
        localStorage.setItem("EmailLogin", EmailLogIn);
        //console.log thông tin nếu hợp lệ
        console.log("Data: ", EmailLogIn, PasswordLogIn)  
    }
    //hàm khi nút Sign up được click
    const onSignUp = (event) => {
        event.preventDefault();
        //validate Email
        if(!FisrtNameSignUp){
            alert("Firstname không hợp lê!")
            return
        }
        if(!LastNameSignUp){
            alert("Lastname không hợp lệ!")
            return
        }
        if(EmailSignUp.match(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/) === null){
            alert("Email không hợp lê!")
            return 
        }
        //validate Password
        if(PasswordSignUp.match(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/) === null){
            alert("Password không hợp lê!")
            return 
        }
        //đẩy thông tin lên local storage nếu thông tin hợp lệ
        localStorage.setItem("LastName", LastNameSignUp)
        localStorage.setItem("FirstName", FisrtNameSignUp);
        localStorage.setItem("Password", PasswordSignUp);
        localStorage.setItem("Email", EmailSignUp);
        //console.log thông tin nếu hợp lệ
        console.log("Data: ", FisrtNameSignUp, LastNameSignUp, EmailSignUp, PasswordSignUp)  
    } 
    //hàm thay đổi sang form sign up 
    const ChangeFormToSignUp = () => {
        console.log("Change form to sign up");
        const {onClickSignUpForm} = props;
        onClickSignUpForm();
        console.log(displaySignUpBoolean)
    }
    // hàm thay đổi sang form login 
    const ChangeFormToLogIn = () => {
        console.log("Change form to login");
        const {onClickLoginForm} = props;
        onClickLoginForm();
        console.log(displayLogInBoolean)
    }
    
    const {displaySignUpBoolean, displayLogInBoolean} = props;
    return(
        <Container fluid>
            <Form className="background-image " >
                <Row  className=" d-flex align-items-center min-vh-100 ">
                    <Col sm={4} className="mx-auto  form-background">
                        <Row className = "my-3">
                            <Col sm={12}  className = "my-2">
                                {displaySignUpBoolean ? <Button className="w-50 button" >SignUp</Button> : <Button className="w-50 button-nonActive" onClick={ChangeFormToSignUp}>SignUp</Button>}
                                {displayLogInBoolean ? <Button className="w-50 button">Log In</Button>: <Button className="w-50 button-nonActive" onClick={ChangeFormToLogIn} >Log In</Button>}
                            </Col>
                            {displaySignUpBoolean?  //sử dụng condition (ternery) operator để hiển thị form sign up
                            // SignUp Display 
                            <div>
                                <Col sm={12} className = "my-2">
                                    <h4 className="text-center" style={{color:"white", fontWeight: "lighter"}}>
                                        Sign Up for Free
                                    </h4>
                                </Col>
                                <Row className="mt-3">
                                    <Col sm={6} style={{height:"60px"}}>
                                        <FormGroup>
                                            <Input value={FisrtNameSignUp}  id="firstname" name="firstname" required="required" onChange={onChangeFirstNameSignUp}></Input>
                                            <Label htmlFor="firstname">First Name</Label>
                                        </FormGroup>
                                    </Col>      
                                    <Col sm={6} style={{height:"60px"}}>
                                        <FormGroup>
                                            <Input value = {LastNameSignUp} id="lastname" name="lastname" required="required" onChange={ onChangeLastNameSignUp}></Input>
                                            <Label htmlFor="lastname">Last Name</Label>
                                        </FormGroup>
                                    </Col> 
                                </Row>
                                <Col sm={12} style={{height:"60px"}} >
                                    <FormGroup>
                                        <Input value = {EmailSignUp}  id="email" name="email" required="required"  onChange={onChangeEmailSignUp}></Input>
                                        <Label htmlFor="email">Email Address</Label>
                                    </FormGroup>
                                </Col>
                                <Col sm={12} style={{height:"60px"}}>
                                    <FormGroup>
                                        <Input value= {PasswordSignUp} type="password"  id="password"  name="password" required="required" onChange={onChangePasswordSignUp}></Input>
                                        <Label htmlFor="password">Password</Label>
                                    </FormGroup>
                                </Col>
                                <Col sm={12} className="d-flex justify-content-end">
                                    <a href="/">Forgot Password?</a>
                                </Col>
                                <Col sm={12} className="my-3">
                                    <Button className="w-100 button" style={{color:"white", fontWeight: "lighter", backgroundColor:"rgb(9, 173, 118)"}} type="submit" onClick={onSignUp}>
                                        <h3>Sign Up</h3>
                                    </Button>
                                </Col>
                            </div>
                            : //Login Display
                            <div>
                                <Col sm={12} className = "my-2">
                                    <h4 className="text-center" style={{color:"white", fontWeight: "lighter"}}>
                                        Welcome Back!
                                    </h4>
                                </Col>
                                <Col sm={12} className = "mt-3" style={{height:"60px"}} >
                                    <FormGroup>
                                        <Input value={EmailLogIn} id="email" name="email" required="required" onChange={onChangeEmail}></Input>
                                        <Label htmlFor="email">Email Address</Label>
                                    </FormGroup>
                                </Col>
                                <Col sm={12} style={{height:"60px"}}>
                                    <FormGroup>
                                        <Input type="password" value ={PasswordLogIn} id="password"  name="password" required="required" onChange={onChangePassword}></Input>
                                        <Label htmlFor="password">Password</Label>
                                    </FormGroup>
                                </Col>
                                <Col sm={12} className="d-flex justify-content-end">
                                    <a href="/">Forgot Password?</a>
                                </Col>
                                <Col sm={12} className="my-3">
                                    <Button className="w-100 button" style={{color:"white", fontWeight: "lighter", backgroundColor:"rgb(9, 173, 118)"}} type="submit" onClick={onLogIn}>
                                        <h3>LOG IN</h3>
                                    </Button>
                                </Col>
                            </div>
                            } 
                        </Row>
                    </Col>
                </Row>
            </Form>
        </Container>
    )
}

export default Form1